from flask import Flask, make_response, jsonify

app = Flask(__name__)

@app.route("/")
def index():
    return make_response(jsonify({"version": "2.0.0"}))

if __name__ == "__main__":
    app.run(port=5000, host="0.0.0.0")